interface IRespuesta {
  a: string;
  b: string;
  c: string;
}


export interface IPregunta {
  id: number;
  pregunta: string;
  respuesta: IRespuesta;
  correcta: string;
}
