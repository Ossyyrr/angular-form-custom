export const response = [

{
  id: 1,
  pregunta: "¿De que color es el cielo?",
  respuesta:{
    a: "verde",
    b: "azul",
    c: "amarillo"
  },
  correcta: "b",
},
{ id: 2,
  pregunta: "¿Cuanto suman 7 + 8?",
  respuesta:{
    a: "3",
    b: "8",
    c: "15"
  },
  correcta: "c",
},
{ id: 3,
  pregunta: "¿Cual es la capital de Italia?",
  respuesta:{
    a: "Roma",
    b: "Venecia",
    c: "Florencia"
  },
  correcta: "a",
},
{ id: 4,
  pregunta: "¿Cuantas patas tiene un gato?",
  respuesta:{
    a: "3",
    b: "4",
    c: "5"
  },
  correcta: "b",
}

];



