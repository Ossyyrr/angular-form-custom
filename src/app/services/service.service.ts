import { Injectable } from '@angular/core';
import { IPregunta } from '../interfaces/api-interface';
import { response } from '../api/api.js';
import { IFormInit } from '../interfaces/form-interface';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  public preguntas: IPregunta[];
  public user: IFormInit;

  constructor(private cookieService: CookieService) {
    this.preguntas = response;
  }

getResponse() {
  return this.preguntas;
}

setUser(user: IFormInit) {
  this.user = user;
}

public checkLogin(): boolean {
  const token = this.cookieService.get('auth-token');
  return !!token;
}
setNewQuestion(question){
  this.preguntas.unshift(question);
  //console.log(this.preguntas);
}
deleteQuiestion(id){
  this.preguntas.forEach((value: IPregunta, key: number)=>{
    if(value.id === id) {
      this.preguntas.splice(key, 1);
    }
  })
}

}
