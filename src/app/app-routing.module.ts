import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormComponent } from './components/form/form.component';
import { TestListComponent } from './components/test-list/test-list.component';
import { ActiveGuard } from './guards/active.guard';
import {AddCuestionComponent} from './components/add-cuestion/add-cuestion.component';

const routes: Routes = [
  { path: '', component: TestListComponent },
 // { path: 'test', component: TestListComponent, canActivate: [ActiveGuard]},
  { path: 'add', component: AddCuestionComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
