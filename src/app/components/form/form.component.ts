import { Component, OnInit } from '@angular/core';
import { ServiceService } from 'src/app/services/service.service';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { IFormInit } from '../../interfaces/form-interface';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  myForm: FormGroup;

  name: string;
  dni: string;
  mail: string;

  constructor(private service: ServiceService, private route: Router, private formbuilder: FormBuilder, private cookie: CookieService) {
    // console.log(service.getResponse());

    this.myForm = this.formbuilder.group({
      name: ['User'],
      dni: [''],
      mail: [''],
    });

  }

  ngOnInit() {
  }

  FormInitSubmit(values: IFormInit) {
    this.service.setUser(values);
    this.route.navigate(['/test']);
  }


}
