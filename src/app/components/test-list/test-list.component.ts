import { Component, OnInit } from '@angular/core';
import { IPregunta } from 'src/app/interfaces/api-interface';
import { ServiceService } from 'src/app/services/service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-test-list',
  templateUrl: './test-list.component.html',
  styleUrls: ['./test-list.component.scss']
})
export class TestListComponent implements OnInit {

  public preguntas: IPregunta[];
  puntuacion: number;

  constructor(private service: ServiceService, private route: Router) {
    this.preguntas = [];
  }
  ngOnInit() {
    this.preguntas = this.service.getResponse();
  }

  finalizarCuestionario() {
    const allRadio = document.getElementsByTagName('input');
    let numArr = 0;
    this.puntuacion = 0;
 
    this.preguntas.forEach((pregunta: IPregunta, key: number) => {
      let corr = pregunta.correcta;
      for (let i = numArr; i < numArr + 3; i++) {
        // console.log(corr);
        if (allRadio[i].checked) {
          // console.log(corr);
          switch (i) {
            case 0 + numArr: if (corr === 'a') this.puntuacion++;
              break;
            case 1 + numArr: if (corr === 'b') this.puntuacion++;
              break;
            case 2 + numArr: if (corr === 'c') this.puntuacion++;
              break;
          }
        }
      } numArr += 3;
    });
    this.puntuacion = this.puntuacion  / this.preguntas.length;
    console.log(this.puntuacion);
  }

  deleteQuestion(id: number): void {
    this.service.deleteQuiestion(id);
  }

  addQuestion(){
    this.route.navigate(['/add']);
  }

}
