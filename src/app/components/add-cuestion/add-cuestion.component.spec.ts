import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCuestionComponent } from './add-cuestion.component';

describe('AddCuestionComponent', () => {
  let component: AddCuestionComponent;
  let fixture: ComponentFixture<AddCuestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCuestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
