import { Component, OnInit } from '@angular/core';
import { IPregunta } from 'src/app/interfaces/api-interface';
import { ServiceService } from 'src/app/services/service.service';
import { Router } from '@angular/router';
import { IFormAdd } from 'src/app/interfaces/form-interface';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-cuestion',
  templateUrl: './add-cuestion.component.html',
  styleUrls: ['./add-cuestion.component.scss']
})
export class AddCuestionComponent implements OnInit {

  public question: IPregunta;
  idMax: number;

  FormAdd: FormGroup;
  id: number;
  pregunta: string;
  a: string;
  b: string;
  c: string;
  correcta: string;


  constructor(private service: ServiceService, private route: Router, private formbuilder: FormBuilder) {

    this.FormAdd = this.formbuilder.group({
      pregunta: [''],
      a: [''],
      b: [''],
      c: [''],
      correcta: ['', Validators.required],  // @TODO: controlar que no se quede vacío
    });

  }
  ngOnInit() {
  }

  FormAddToIpregunta(value: IFormAdd): IPregunta {
    const arr = this.service.getResponse();
    arr.forEach((value: IPregunta, key: number) => {
      const arrId = [];
      arrId.push(value.id);
      this.idMax = Math.max.apply(null, arrId);
    });
    return {
      id: (this.idMax + 1),
      pregunta: value.pregunta,
      correcta: value.correcta,
      respuesta: {
        a: value.a,
        b: value.b,
        c: value.c,
      }
    }
  }

  FormInitSubmit(values: IFormAdd) {
    const question: IPregunta = this.FormAddToIpregunta(values);
    this.service.setNewQuestion(question);
    this.route.navigate(['/']);
    console.log(values);
  }

  volverLista(){
    this.route.navigate(['/']);
  }

}

