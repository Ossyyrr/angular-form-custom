import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ServiceService } from '../services/service.service';

@Injectable({
  providedIn: 'root'
})
export class ActiveGuard implements CanActivate {
  constructor(private service: ServiceService, private route: Router) {}
  canActivate(): boolean {
    const isLoggedIn = this.service.checkLogin();
    if (!isLoggedIn) {
      this.route.navigate(['/']);
      return false;
    }
    return true;
  }

}
